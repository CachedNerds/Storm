CPPFLAGS = --std=c++1z -c
ADDITIONAL_FLAGS = -DBOOST_SYSTEM_NO_DEPRECATED -I ~/boost_1_64_0
LINKER_FLAGS = -lboost_system

all: main.o
	g++ $(LINKER_FLAGS) main.o -o storm

main.o: main.cpp
	g++ $(CPPFLAGS) $(ADDITIONAL_FLAGS) main.cpp

clean:
	rm *.o storm